public class MovieProblem {
    public static class Movie {
        private String title;
        private String studio;
        private String rating;

        Movie(String title, String studio, String rating) {
            this.title = title;
            this.studio = studio;
            this.rating = rating;
        }

        Movie(String title, String studio) {
            this.title = title;
            this.studio = studio;
            this.rating = "PG";
        }

    }

    public static Movie[] getPg(Movie[] arr){
        Movie[] PG_array = new Movie[arr.length];
        int j = 0;
        for( int i=0; i<arr.length ; i++){
            if(arr[i].rating == "PG"){
                PG_array[j] = arr[i];
                j++;
                System.out.println(arr[i].title);
            }
        }

        return PG_array;
    }



    public static void main(String[] args) {

        Movie movie1 = new Movie("abc", "abc", "PG");
        Movie movie2 = new Movie("xyz", "xyz");
        Movie movie3 = new Movie("pqr", "pqr", "NO");
        Movie movie4 = new Movie("rst", "rst", "DF");
        Movie movie5 = new Movie("Casino Royale", "Eon Productions", "PG-13");

        Movie[] arr = new Movie[5];
        arr[0] = movie1;
        arr[1] = movie2;
        arr[2] = movie3;
        arr[3] = movie4;
        arr[4] = movie5;

        //movie1.getPg(arr);
        getPg(arr);

    }
}